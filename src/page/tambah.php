<?php

require '../functions/functions.php';

if (isset($_POST['submit'])){
    if (tambah($_POST) > 0){
        echo "
            <script>
            alert('Sukses Menambah Data!');
            document.location.href = '../../index.php';
            </script>
            ";
    } else {
        echo "
            <script>
            alert('Data Gagal ditambahkan!');
            </script>
            ";
    };
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="../style/main.css">
    <title>Tambah data</title>
</head>
<body class="bg-light">

    <header>
        <div class="jumbotron text-center bg-dark text-white">
            <h1 class="display-4 mb-4">Create Read Update dan Delete</h1>
        </div>
    </header>

    <main>
        <div class="container">
            <div class="row">

                <div class="container">
                    <h1 class="text-center text-uppercase fw-bolder mb-5">Tambah data Produk</h1>

                    <form method="post" action="" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Barang</label>
                            <input placeholder="Masukan Nama Barang" type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                        <div class="mb-3">
                            <label for="deskripsi" class="form-label">Deskripsi Barang</label>
                            <input placeholder="Masukan deskripsi Barang" type="text" class="form-control" id="deskripsi" name="deskripsi" required>
                        </div>
                        <div class="mb-3">
                            <label for="harga" class="form-label">Harga Barang</label>
                            <input placeholder="Masukan Harga Barang" type="number" class="form-control" id="harga" name="harga" required>
                        </div>
                        <div class="mb-3">
                            <label for="gambar" class="form-label">Foto</label>
                            <input type="file" class="form-control" id="gambar" name="gambar">
                            <label class="disable fst-italic fw-bolder">Gambar yang di dukung (jpg, jpeg, png).</label>
                            <label class="disable fst-italic fw-bolder">Ukuran File yang di dukung Maks 1MB.</label>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Tambah data</button>
                    </form>

                </div>

            </div>
        </div>
    </main>

    <footer></footer>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>
</html>