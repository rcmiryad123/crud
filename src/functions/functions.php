<?php

// Koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "dumbways");

// Fungsi ambil data
function getData ($kueri) {

    global $conn;

    
    $result     = mysqli_query($conn, $kueri);
    $rows       = [];
    while($row  = mysqli_fetch_assoc($result)){
        $rows[] = $row;
    }

    return $rows;

}

function tambah ($data) {

    global $conn;

    // Get Data
    $nama   = htmlspecialchars($data['nama']);
    $deskripsi = htmlspecialchars($data['deskripsi']);
    $harga  = htmlspecialchars($data['harga']);

    // Upload Gambar
    $gambar = upload();

    if (!$gambar) {
        return false;
    }

    $query  = "  INSERT INTO produk (nama, deskripsi, harga, gambar)
                VALUES ('$nama', '$deskripsi', '$harga', '$gambar')";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);

}

function ubah ($data) {

    global $conn;

    $id         = $data['id'];
    $nama       = htmlspecialchars($data['nama']);
    $deskripsi  = htmlspecialchars($data['deskripsi']);
    $harga      = htmlspecialchars($data['harga']);
    $gambarLama = $data['gambar-lama'];

    // Mengecek apakah user Pilih Gambar baru atau tidak
    if ($_FILES['gambar']['error'] === 4){
        
        $gambar = $gambarLama;

    } else {

        $gambar = upload();

    }

    $query = "  UPDATE produk SET
                nama        = '$nama',
                deskripsi   = '$deskripsi',
                harga       = '$harga',
                gambar      = '$gambar'
                WHERE id    = $id
             ";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);

}

function hapus ($id) {

    global $conn;

    mysqli_query($conn, "DELETE FROM produk WHERE id = $id");

    return mysqli_affected_rows($conn);

}

function cari ($keyword) {
    
    $query = "
                SELECT * FROM produk
                WHERE nama LIKE '%$keyword%' OR
                deskripsi LIKE '%$keyword%' OR
                harga LIKE '%$keyword%'
             ";

    return getData($query);

}

function upload () {

    $namaFile   = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error      = $_FILES['gambar']['error'];
    $tmpNama    = $_FILES['gambar']['tmp_name'];

    // Mengecek jika tidak ada File yang di Upload
    if ($error === 4) {
        echo  "
                <script>
                alert('Harap Masukan Gambar');
                </script>
                ";

        return false;

    }

    // Mengecek apakah file yang di input Gambar atau Bukan.
    $eksGambarValid = ['jpg', 'png', 'jpeg'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));

    if (!in_array($ekstensiGambar, $eksGambarValid)){

        echo    "
                <script>
                alert('Foto yang upload tidak didukung, Gunakan Format jpg, jpeg, dan png!');
                </script>
                ";
        
        return false;
    }

    // Mengecek apakah File yang di input diatas 1MB
    if($ukuranFile > 1000000){
        echo    "
                <script>
                alert('Ukuran Gambar terlalu besar, Ukuran harus dibawah 1MB!');
                </script>
                ";
        return false;
    };

    // Jika Lolos pengecekan lakukan
    // Generate nama Baru untuk file Gambar
    $namaFileBaru = uniqid(). '.' . $ekstensiGambar;

    // Pindahkan File
    move_uploaded_file($tmpNama, '../img/'.$namaFileBaru);
    
    return $namaFileBaru;
}


?>