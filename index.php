<?php

require "src/functions/functions.php";

$produks = getData("SELECT * FROM produk");

if (isset($_POST['cari'])){

    $produks = cari($_POST['keyword']);

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="src/style/main.css">
    <title>Create Read Update Delete</title>
</head>
<body class="bg-light">

<form method="post" action="" >
        
    <header>
        
        <div class="jumbotron text-center bg-dark text-white">
            <h1 class="display-4">Create Read Update dan Delete</h1>
            <div class="container">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Ketikan nama Barang disini" name="keyword" id="keyword">
                    <button class="btn btn-outline-light" type="submit" name="cari" id="cari">Cari!</button>
                </div>
            </div>
            <a href="src/page/tambah.php" class="btn btn-primary btn-block">Tambah Barang</a>
        </div>
        
    </header>

    <main>
        
        <div class="container">

            <div class="row">
                <h1 class="display-4 text-center text-uppercase fw-bolder mb-5">Daftar Produk</h1>

                <?php foreach($produks as $pdk) : ?>
                    <div class="col-md-4 mt-3">
                        <div class="card shadow p-3 mb-5 bg-white rounded">
                            <img src="src/img/<?= $pdk['gambar'] ?>" style="height: 14rem;" class="card-img-top">
                            <div class="card-body">
                            <h5 class="card-title"><?= $pdk['nama'] ?></h5>
                            <div class="mt-3 mb-4">
                                <div><?= $pdk['deskripsi']; ?></div>
                                <div class="mt-3 fw-bold">Rp. <?= $pdk['harga']; ?></div>
                            </div>
                            <a href="src/page/ubah.php?id=<?= $pdk['id'] ?>" id="tombol-ubah" name="ubah" type="submit" class="btn btn-primary">Ubah</a>
                            <a href="src/page/hapus.php?id=<?= $pdk['id'] ?>" id="tombol-hapus" name="hapus" type="submit" class="btn btn-danger">Hapus!</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>

    </main>

    <footer></footer>

</form>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>
</html>